package living.smarthack.smarthome.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import living.smarthack.smarthome.R;
import living.smarthack.smarthome.adapter.AccessWorkerAdapter;
import living.smarthack.smarthome.adapter.RecyclerItemClickListener;
import living.smarthack.smarthome.model.AccessWorker;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcccessControlFragment extends Fragment {

    private ArrayList<AccessWorker> list = new ArrayList<AccessWorker> () ;
    private AccessWorkerAdapter adapter ;
    private RecyclerView recyclerView;
    private String param = "" ;
    public AcccessControlFragment() {
        // Required empty public constructor
    }


    public static AcccessControlFragment newInstance( String param) {
        Bundle args = new Bundle();
        args.putString("flag" , param );
        AcccessControlFragment fragment = new AcccessControlFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null ){
            String argu = getArguments().getString("flag" , "");
            if(!argu.isEmpty()){
                param = argu ;
            }
        }

    }

    public void fetchWorkers() {
        //list.clear();
        System.out.println("nfc add 2" );
        AccessWorker worker = new AccessWorker (  0 , "Jeffery" , "9f872687skjndasiygq8729s") ;
        adapter.addItem(worker );
;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_acccess_control, container, false);
        recyclerView = view.findViewById(R.id.listAccessControl) ;
        RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager( getActivity()) ;
        recyclerView.setLayoutManager( layoutManager );
        adapter = new AccessWorkerAdapter(getActivity().getApplicationContext() , list );
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getApplicationContext(),
                        recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        AccessWorker selectedWorker = list.get(position);
                        Toast.makeText( getActivity().getApplicationContext()  ,
                                selectedWorker.toString() , Toast.LENGTH_SHORT    ).show() ;
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        recyclerView.setAdapter(adapter);
        final SwipeRefreshLayout swipeLayout = view.findViewById(R.id.layoutRefresh) ;
        swipeLayout.setRefreshing(false ) ;
        SwipeRefreshLayout.OnRefreshListener listener = new SwipeRefreshLayout.OnRefreshListener () {
            @Override
            public void onRefresh() {
                fetchWorkers();
                adapter.notifyDataSetChanged();
                swipeLayout.setRefreshing(false ) ;
            }
        };

        swipeLayout.setOnRefreshListener(listener);
        //fetchWorkers() ;
        SharedPreferences pref = getActivity().getSharedPreferences(
                getActivity().getPackageName() , Context.MODE_PRIVATE);
        String val = pref.getString("flag" , "") ;
        if(!val.isEmpty()) {
            System.out.println("testing") ;
            fetchWorkers();
        }

        return view ;
    }

}
