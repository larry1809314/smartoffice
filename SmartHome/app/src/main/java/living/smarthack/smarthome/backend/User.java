package living.smarthack.smarthome.backend;

public  class User {
    private static  User sharedInstance  = null;
    public String login_name = "Larry Lo" ;
    public String login_password = "" ;
    public String telephone = "+85218278192" ;
    public String email = "larry.lo@gmail.com" ;


    private User() {
        // Exists only to defeat instantiation.
    }

    public static User getInstance() {
        if(sharedInstance == null) {
            sharedInstance = new User();
        }
        return sharedInstance;
    }

    public void login() {

    }


    public void logout () {
        login_name = "";
        login_password = "";
        telephone = "" ;
        email = "" ;
    }



}

