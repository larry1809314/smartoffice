package living.smarthack.smarthome.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import living.smarthack.smarthome.R;
import living.smarthack.smarthome.model.AccessWorker;
import living.smarthack.smarthome.model.Room;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolders>  {

    private ArrayList<Room> itemList;
    private Context context;
    private int lastPosition = -1;

    public RoomAdapter(Context context , ArrayList<Room> itemList) {
        this.itemList = itemList;
        this.context = context;

    }

    public void setList(ArrayList<Room> rooms) {
        this.itemList = rooms;
    }
    @NonNull
    @Override
    public RoomAdapter.ViewHolders onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_room, null);
        return new ViewHolders(layoutView);

    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdapter.ViewHolders viewHolders, int i) {
        Room element = itemList.get(i) ;
        Log.e("ASDADASD", element.getRoom_name());
        viewHolders.username.setText( element.getRoom_name()) ;
        viewHolders.desp.setText( element.getRoom_location()) ;
        viewHolders.image.setImageResource(R.drawable.buildings);
        setAnimation(viewHolders.itemView,  i);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void addItem(Room room) {
        itemList.add(itemList.size() - 1, room) ;
        notifyDataSetChanged();
    }




    public class ViewHolders extends RecyclerView.ViewHolder {

        private TextView username;
        private TextView desp ;
        private ImageView image  ;

        public ViewHolders(View itemView) {
            super(itemView);
            username =  itemView.findViewById(R.id.lblTitle);
            desp = itemView.findViewById(R.id.lblDesp);
            image = itemView.findViewById(R.id.imgType);
        }

    }
}



