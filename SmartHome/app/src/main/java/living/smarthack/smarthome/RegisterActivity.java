package living.smarthack.smarthome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import living.smarthack.smarthome.adapter.StaticData;

public class RegisterActivity extends AppCompatActivity {

    public static final String MIME_TEXT_PLAIN = "text/plain";

    private EditText login_username, login_password,
    login_name, login_phone, login_email;

    private TextView login_code;
    private Button bt_register;
    private String TAG = "SmartHome";

    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    Tag myTag;
    Boolean writeMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        login_username = findViewById(R.id.login_username);
        login_password = findViewById(R.id.login_password);
        login_name = findViewById(R.id.login_name);
        login_code = findViewById(R.id.login_code);
        login_phone = findViewById(R.id.login_phone);
        login_email = findViewById(R.id.login_email);

        bt_register = findViewById(R.id.btnRegister);


        login_username.setText("larry");
        login_password.setText("test123");
        login_name.setText("Larry Lo");
        login_phone.setText("+85218278192");
        login_email.setText("larry.lo@gmail.com");
        setup();

    }

    private void setup() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_SHORT).show();
            return;

        }

        if (!nfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC disabled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NFC enabled", Toast.LENGTH_SHORT).show();
        }

        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };

        bt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //----- push to register
                checkRegisteration();
            }
        });


    }


    private void checkRegisteration() {
        String username = login_username.getText().toString();
        String password = login_password.getText().toString();
        String phone = login_phone.getText().toString();
        String name = login_name.getText().toString();
        String email = login_email.getText().toString();
        String nfc_code = login_code.getText().toString();

        boolean ok = false;
        if (username.length() == 0) {
            ok = false;
            showDialog("Username cannot empty", "Error");

        } else {
            ok = true;
        }

        if (ok) {
            if (password.length() == 0) {
                ok = false;
                showDialog("Password cannot empty", "Error");

            } else {
                ok = true;
            }
        }

        if (ok) {
            if (phone.length() == 0) {
                ok = false;
                showDialog("Please fill your phone", "Error");

            } else {
                ok = true;
            }
        }

        if (ok) {
            if (email.length() == 0) {
                ok = false;
                showDialog("Please Fill your email", "Error");

            } else {
                ok = true;
            }
        }

        if (ok) {
            if (name.length() == 0) {
                ok = false;
                showDialog("Please Fill Your name", "Error");
            } else {
                ok = true;
            }
        }

        if (ok) {
            if (nfc_code.length() == 0) {
                ok = false;
                showDialog("Please put the NFC Device", "Error");
            } else {
                ok = true;
            }
        }


        if (ok) {
            // ---- send
            RequestData requestData = new RequestData();
            Register register = new Register();
            register.setLogin_code(login_code.getText().toString());
            register.setLogin_email(login_email.getText().toString());
            register.setLogin_name(login_name.getText().toString());
            register.setLogin_password(login_password.getText().toString());
            register.setLogin_phone(login_phone.getText().toString());
            register.setLogin_username(login_username.getText().toString());

            requestData.setData_for(2);
            requestData.setRegister(register);

            String data = new Gson().toJson(requestData);

            //new SendData(data).execute();

            try {
                GsonBuilder gsonBuilder = new GsonBuilder();
                final Gson gson = gsonBuilder.create();
                byte[] datas = data.getBytes("UTF-8");
                String base64 = Base64.encodeToString(datas, Base64.DEFAULT);

                String url = StaticData.serverURL + "/livingBackend?data=" + base64;
                Log.e(TAG, url);
                AndroidNetworking.get(url)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String resp = response.toString();
                                    Log.e(TAG, resp);
                                    ResponseData responseData = new Gson().fromJson(resp,
                                            new TypeToken<ResponseData>(){}.getType());

                                    Log.e(TAG, responseData.getStat() + ", " + responseData.getMesg());
                                    if (responseData.getStat() == 1) {
                                        finish();
                                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                    } else {
                                        showDialog(responseData.getMesg(), "Error");
                                    }

                                }catch ( Exception ex) {
                                    ex.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            } catch (Exception ee) {
                ee.printStackTrace();
            }

        }

    }


    private class RequestData {
        int data_for;
        Register register;

        public int getData_for() {
            return data_for;
        }

        public void setData_for(int data_for) {
            this.data_for = data_for;
        }

        public Register getRegister() {
            return register;
        }

        public void setRegister(Register register) {
            this.register = register;
        }
    }

    private class Register {
        String login_name, login_password,
                login_username, login_phone,
                login_code, login_email;

        public String getLogin_name() {
            return login_name;
        }

        public void setLogin_name(String login_name) {
            this.login_name = login_name;
        }

        public String getLogin_password() {
            return login_password;
        }

        public void setLogin_password(String login_password) {
            this.login_password = login_password;
        }

        public String getLogin_username() {
            return login_username;
        }

        public void setLogin_username(String login_username) {
            this.login_username = login_username;
        }

        public String getLogin_phone() {
            return login_phone;
        }

        public void setLogin_phone(String login_phone) {
            this.login_phone = login_phone;
        }

        public String getLogin_code() {
            return login_code;
        }

        public void setLogin_code(String login_code) {
            this.login_code = login_code;
        }

        public String getLogin_email() {
            return login_email;
        }

        public void setLogin_email(String login_email) {
            this.login_email = login_email;
        }
    }


    private class SendData extends AsyncTask<Void, Void, Boolean> {
        MaterialDialog dialog;
        boolean ret = false;
        private String data;
        private String mesg;

        SendData(String data) {
            this.data = data;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MaterialDialog.Builder(RegisterActivity.this)
                    .title("Registering...")
                    .progress(true, 100)
                    .canceledOnTouchOutside(false)
                    .cancelable(false)
                    .show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {


            return ret;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (!result) {
                showDialog("Registration fail, " + mesg, "Error");
            } else {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void showDialog( String mesg,  String title) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(mesg)
                .positiveText("OK")
                .show();


    }
    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }

            byte[] tagId = getIntent().getByteArrayExtra(NfcAdapter.EXTRA_ID);
            Log.i("EHEHEHEHEHE",tagId + "");
            String hexdump = new String();
            for (int i = 0; i < tagId.length; i++) {
                String x = Integer.toHexString(((int) tagId[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                hexdump += x + ' ';
            }
            Log.i("tag serial key",hexdump);

            login_code.setText(hexdump.replaceAll(" ",""));
            buildTagViews(msgs);
        }
    }

    private void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        try {
            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //Toast.makeText(this, "NFC Content: " + text   , Toast.LENGTH_LONG).show();

    }



    @Override
    protected void onResume() {
        super.onResume();
        WriteModeOn();
    }

    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }



    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        readFromIntent(intent);
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }


    private void WriteModeOn(){
        writeMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff(){
        writeMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }


    private class ResponseData {

        int stat;
        String mesg;

        public int getStat() {
            return stat;
        }

        public void setStat(int stat) {
            this.stat = stat;
        }

        public String getMesg() {
            return mesg;
        }

        public void setMesg(String mesg) {
            this.mesg = mesg;
        }



    }





}
