package living.smarthack.smarthome.model;


import java.util.ArrayList;

public class Device {
    double sensor_temperature, sensor_humidity;
    int device_type; // 1 = tempereature, 2 = humidity, 3 = light, 4 = door;
    int device_stat;
    String device_name, device_type_name;
    int device_id;

    public String getDevice_type_name() {
        return device_type_name;
    }

    public void setDevice_type_name(String device_type_name) {
        this.device_type_name = device_type_name;
    }

    public double getSensor_temperature() {
        return sensor_temperature;
    }

    public void setSensor_temperature(double sensor_temperature) {
        this.sensor_temperature = sensor_temperature;
    }

    public double getSensor_humidity() {
        return sensor_humidity;
    }

    public void setSensor_humidity(double sensor_humidity) {
        this.sensor_humidity = sensor_humidity;
    }

    public int getDevice_type() {
        return device_type;
    }

    public void setDevice_type(int device_type) {
        this.device_type = device_type;
    }

    public int getDevice_stat() {
        return device_stat;
    }

    public void setDevice_stat(int device_stat) {
        this.device_stat = device_stat;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

}
