package living.smarthack.smarthome.model;




public class Room {
    String room_name, room_id, room_location;

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getRoom_location() {
        return room_location;
    }

    public void setRoom_location(String room_location) {
        this.room_location = room_location;
    }




    @Override
    public String toString() {
        //return "Task [id=" + id + ", type=" + type  + ", description=" + description + "]";
        return "";
    }

}
