package living.smarthack.smarthome.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import living.smarthack.smarthome.R;
import living.smarthack.smarthome.model.AccessWorker;

public class AccessWorkerAdapter extends RecyclerView.Adapter<AccessWorkerAdapter.ViewHolders>  {

    private ArrayList<AccessWorker> itemList;
    private Context context;
    private int lastPosition = -1;

    public AccessWorkerAdapter(Context context , ArrayList<AccessWorker> itemList) {
        this.itemList = itemList;
        this.context = context;

    }

    @NonNull
    @Override
    public AccessWorkerAdapter.ViewHolders onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_access_worker, null);
        return new ViewHolders(layoutView);

    }

    @Override
    public void onBindViewHolder(@NonNull AccessWorkerAdapter.ViewHolders viewHolders, int i) {
        AccessWorker element = itemList.get(i) ;
        viewHolders.username.setText( element.getName()) ;
        setAnimation(viewHolders.itemView,  i);
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void addItem(AccessWorker worker) {
        itemList.add(worker);
        notifyDataSetChanged();
    }


    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public class ViewHolders extends RecyclerView.ViewHolder {

        private TextView username;
        private ImageView image  ;


        public ViewHolders(View itemView) {
            super(itemView);
            username =  itemView.findViewById(R.id.lblUsername);
            image = itemView.findViewById(R.id.imgKey);
        }

    }
}



