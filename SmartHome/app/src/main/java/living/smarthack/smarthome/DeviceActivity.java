package living.smarthack.smarthome;

import android.content.res.ColorStateList;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;

import living.smarthack.smarthome.adapter.DashboardAdapter;
import living.smarthack.smarthome.adapter.RecyclerItemClickListener;
import living.smarthack.smarthome.adapter.StaticData;
import living.smarthack.smarthome.model.Device;
import living.smarthack.smarthome.model.RequestData;
import living.smarthack.smarthome.model.ResponseData;

public class DeviceActivity extends AppCompatActivity {

    private ArrayList<Device> list = new ArrayList<> () ;
    private DashboardAdapter adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        list = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.listDashboard) ;
        RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager(  this ) ;
        recyclerView.setLayoutManager( layoutManager );
        adapter = new DashboardAdapter(DeviceActivity.this , list );
        recyclerView.setAdapter(adapter);

        fetchDevice ();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        Device selectedDevice = list.get(position);
                        if (selectedDevice.getDevice_type() == 3) {
                            Request requestData = new Request();
                            if (selectedDevice.getDevice_stat() == 0) {
                                selectedDevice.setDevice_stat(1);
                                requestData.setDevice_id(selectedDevice.getDevice_id());
                                requestData.setDevice_stat(selectedDevice.getDevice_stat());
                                requestData.setDevice_type(3);
                                setDeviceStat(requestData);

                            } else if (selectedDevice.getDevice_stat() == 1) {
                                selectedDevice.setDevice_stat(0);
                                requestData.setDevice_id(selectedDevice.getDevice_id());
                                requestData.setDevice_stat(selectedDevice.getDevice_stat());
                                requestData.setDevice_type(3);
                                setDeviceStat(requestData);
                            }
                        } else if (selectedDevice.getDevice_type() == 4) {
                            Request requestData = new Request();
                            if (selectedDevice.getDevice_stat() == 0) {
                                selectedDevice.setDevice_stat(1);
                                requestData.setDevice_id(selectedDevice.getDevice_id());
                                requestData.setDevice_stat(selectedDevice.getDevice_stat());
                                requestData.setDevice_type(4);
                                setDeviceStat(requestData);

                            } else if (selectedDevice.getDevice_stat() == 1) {
                                selectedDevice.setDevice_stat(0);
                                requestData.setDevice_id(selectedDevice.getDevice_id());
                                requestData.setDevice_stat(selectedDevice.getDevice_stat());
                                requestData.setDevice_type(4);
                                setDeviceStat(requestData);
                            }
                        }


                        adapter.notifyDataSetChanged();
                        /*
                        if(selectedDevice .getType() == "light") {
                            if(selectedDevice.x_position == 0 ){
                                if (selectedDevice.y_position == "Status: ON") {
                                    selectedDevice.y_position = "Status: OFF";
                                }else if (selectedDevice.y_position == "Status: OFF") {
                                    selectedDevice.y_position = "Status: ON";
                                }
                            }else{
                                String statement = selectedDevice.getDescription() + " is NOT CONNECTED" ;
                                Toast.makeText(DeviceActivity.this,
                                        statement , Toast.LENGTH_SHORT).show();
                            }

                        }else if(selectedDevice .getType() == "door") {
                            if (selectedDevice.y_position == "Status: UNLOCK") {
                                selectedDevice.y_position = "Status: LOCK";
                            }else if (selectedDevice.y_position == "Status: LOCK") {
                                selectedDevice.y_position = "Status: UNLOCK";
                            }
                        }

                        adapter.notifyDataSetChanged();

//                        Toast.makeText( getApplicationContext()  ,
//                                selectedDevice.toString() , Toast.LENGTH_SHORT    ).show() ;
                        */
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );




        final SwipeRefreshLayout swipeLayout = findViewById(R.id.layoutRefresh) ;
        swipeLayout.setRefreshing(false ) ;
        SwipeRefreshLayout.OnRefreshListener listener = new SwipeRefreshLayout.OnRefreshListener () {

            @Override
            public void onRefresh() {
                fetchDevice();
                adapter.notifyDataSetChanged();
                swipeLayout.setRefreshing(false ) ;
            }
        };
        swipeLayout.setOnRefreshListener(listener);

    }

    private void setDeviceStat(Request requestData) {
        try {
            String data = new Gson().toJson(requestData);
            byte[] datas = data.getBytes("UTF-8");
            String base64 = Base64.encodeToString(datas, Base64.DEFAULT);

            String url = StaticData.serverURL + "/DeviceStat?data=" + base64;
            AndroidNetworking.get(url)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String resp = response.toString();
                                Log.i("ASDASDASD", resp);

                            }catch ( Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected  void fetchDevice() {
        list.clear();
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            final Gson gson = gsonBuilder.create();
            RequestData requestData = new RequestData();
            requestData.setLogin_id(StaticData.loginID);
            requestData.setRoom_id(StaticData.roomID);

            String data = new Gson().toJson(requestData);

            byte[] datas = data.getBytes("UTF-8");
            String base64 = Base64.encodeToString(datas, Base64.DEFAULT);

            String url = StaticData.serverURL + "/DeviceSensor?data=" + base64;
            AndroidNetworking.get(url)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String resp = response.toString();
                                Log.i("ASDASDASD", resp);
                                DeviceActivity.ResponseData responseData = new Gson().fromJson(resp,
                                        new TypeToken<DeviceActivity.ResponseData>(){}.getType());

                                if (responseData.getStat() == 1) {
                                    list = responseData.getDeviceDatas();
                                    adapter.setList(list);
                                    adapter.notifyDataSetChanged();
                                }

                            }catch ( Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }


    private class ResponseData {

        int stat;
        ArrayList<Device> deviceDatas;

        public int getStat() {
            return stat;
        }

        public void setStat(int stat) {
            this.stat = stat;
        }

        public ArrayList<Device> getDeviceDatas() {
            return deviceDatas;
        }

        public void setDeviceDatas(ArrayList<Device> deviceDatas) {
            this.deviceDatas = deviceDatas;
        }

    }



    private class Request {
        int device_id;
        int device_type;
        int device_stat;

        public int getDevice_id() {
            return device_id;
        }

        public void setDevice_id(int device_id) {
            this.device_id = device_id;
        }

        public int getDevice_type() {
            return device_type;
        }

        public void setDevice_type(int device_type) {
            this.device_type = device_type;
        }

        public int getDevice_stat() {
            return device_stat;
        }

        public void setDevice_stat(int device_stat) {
            this.device_stat = device_stat;
        }
    }

}
