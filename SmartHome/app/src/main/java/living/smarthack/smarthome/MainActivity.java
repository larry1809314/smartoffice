package living.smarthack.smarthome;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.UnsupportedEncodingException;

import living.smarthack.smarthome.fragment.AcccessControlFragment;
import living.smarthack.smarthome.fragment.DashboardFragment;
import living.smarthack.smarthome.fragment.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "NfcDemo";

    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    Tag myTag;
    Boolean writeMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateUI();
        setup() ;
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
        WriteModeOn();
    }

    private void setup(){

        SharedPreferences pref = this.getSharedPreferences(getPackageName() , Context.MODE_PRIVATE);
        pref.edit().putString("flag" , "").apply();


        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            return;

        }

        if (!nfcAdapter.isEnabled()) {
            //Toast.makeText(this, "NFC disabled", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(this, "NFC enabled", Toast.LENGTH_LONG).show();
        }

        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };
    }


    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }

            byte[] tagId = getIntent().getByteArrayExtra(NfcAdapter.EXTRA_ID);
            Log.i("EHEHEHEHEHE",tagId + "");
            String hexdump = new String();
            for (int i = 0; i < tagId.length; i++) {
                String x = Integer.toHexString(((int) tagId[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                hexdump += x + ' ';
            }
            Log.i("tag serial key",hexdump);

            buildTagViews(msgs);
        }
    }

    private void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        try {
            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            System.out.println("nfc add");

            SharedPreferences pref = this.getSharedPreferences(getPackageName() , Context.MODE_PRIVATE);
            pref.edit().putString("flag" , "access").apply();

            FragmentTransaction  fragTransaction = getSupportFragmentManager().beginTransaction();
            AcccessControlFragment fragment =  new AcccessControlFragment();
            fragTransaction.replace(R.id.container, fragment ).commit() ;
            BottomBar bottomBar = this.findViewById(R.id.bottomBar);
            bottomBar.selectTabWithId(R.id.bb_menu_key);



        } catch (UnsupportedEncodingException e) {
            System.out.println("nfc cannot add");
            e.printStackTrace();
        }

        //Toast.makeText(this, "NFC Content: " + text   , Toast.LENGTH_LONG).show();

    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        readFromIntent(intent);
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }


    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }



    private void WriteModeOn(){
        writeMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff(){
        writeMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }




    private void updateUI () {

        BottomBar bottomBar = this.findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                FragmentTransaction  fragTransaction = getSupportFragmentManager().beginTransaction();

                if (tabId == R.id.bb_menu_dashboard) {
                    fragTransaction.replace(R.id.container, new DashboardFragment()).commit() ;
                    //setActionBarTitle ("租倉總覽")
                    //setActionBarSearch();
                } else  if (tabId == R.id.bb_menu_key) {
                    fragTransaction.replace(R.id.container, new AcccessControlFragment()).commit() ;
                    //setActionBarTitle(resources.getString(R.string.favor))
                    //setActionBarTitle(resources.getString(R.string.setting_my))
                } else  if (tabId == R.id.bb_menu_user) {
                    fragTransaction.replace(R.id.container, new ProfileFragment()).commit() ;
                    //setActionBarTitle(resources.getString(R.string.favor))
                    //setActionBarTitle(resources.getString(R.string.setting_my))
                }
            }
        });


    }


}
