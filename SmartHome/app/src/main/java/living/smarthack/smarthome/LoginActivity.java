package living.smarthack.smarthome;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import living.smarthack.smarthome.adapter.StaticData;

public class LoginActivity extends AppCompatActivity {

    private TextView mUsername, mPassword;
    private Button loginButton, registerButton;
    private View loginView, progressView;

    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    Tag myTag;
    Boolean writeMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginView = findViewById(R.id.loginView);
        progressView = findViewById(R.id.login_progress);

        loginButton = findViewById(R.id.btnLogin);
        registerButton = findViewById(R.id.btnRegister);

        mUsername = findViewById(R.id.input_username);
        mPassword = findViewById(R.id.input_password);
        mUsername.setText ("larry");
        mPassword .setText ("test123");

        loginView.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.GONE);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                finish();
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginProcess();
            }
        });
        setup();
    }


    private void setup() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_SHORT).show();
            return;

        }

        if (!nfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC disabled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NFC enabled", Toast.LENGTH_SHORT).show();
        }

        readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };

    }

    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }

            byte[] tagId = getIntent().getByteArrayExtra(NfcAdapter.EXTRA_ID);
            Log.i("EHEHEHEHEHE",tagId + "");
            String hexdump = new String();
            for (int i = 0; i < tagId.length; i++) {
                String x = Integer.toHexString(((int) tagId[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                hexdump += x + ' ';
            }
            Log.i("tag serial key",hexdump);

            loginProcess();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        WriteModeOn();
    }

    @Override
    public void onPause(){
        super.onPause();
        WriteModeOff();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        readFromIntent(intent);
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }


    private void WriteModeOn(){
        writeMode = true;
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    private void WriteModeOff(){
        writeMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }



    private void loginProcess() {

        boolean ok = false;

        if (mUsername.getText().toString().length() > 0) {
            ok = true;
        }

        if (ok) {
            if (mPassword.getText().toString().length() == 0) {
                ok = false;
            }
        }


        if (ok) {
            loginView.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.GONE);
            registerButton.setVisibility(View.GONE);
            try {
                GsonBuilder gsonBuilder = new GsonBuilder();
                final Gson gson = gsonBuilder.create();
                DataExchange dataExchange = new DataExchange();
                dataExchange.setData_for(1);
                Login login = new Login();
                login.setLogin_password(mPassword.getText().toString());
                login.setLogin_username(mUsername.getText().toString());

                dataExchange.setLogin(login);
                String data = new Gson().toJson(dataExchange);

                byte[] datas = data.getBytes("UTF-8");
                String base64 = Base64.encodeToString(datas, Base64.DEFAULT);

                String url = StaticData.serverURL + "/livingBackend?data=" + base64;
                AndroidNetworking.get(url)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String resp = response.toString();
                                    ResponseData responseData = new Gson().fromJson(resp,
                                            new TypeToken<ResponseData>(){}.getType());

                                    if (responseData.getStat() == 1) {
                                        StaticData.loginID = Integer.parseInt(responseData.getMesg());
                                        finish();
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    } else {
                                        loginView.setVisibility(View.VISIBLE);
                                        progressView.setVisibility(View.GONE);
                                        showDialog(responseData.getMesg(), "Error");
                                    }

                                }catch ( Exception ex) {
                                    ex.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

    }

    private class Login {

        String login_username, login_password;

        public String getLogin_username() {
            return login_username;
        }

        public void setLogin_username(String login_username) {
            this.login_username = login_username;
        }

        public String getLogin_password() {
            return login_password;
        }

        public void setLogin_password(String login_password) {
            this.login_password = login_password;
        }

    }

    private class DataExchange {

        int data_for;

        Login login;

        public int getData_for() {
            return data_for;
        }

        public void setData_for(int data_for) {
            this.data_for = data_for;
        }

        public Login getLogin() {
            return login;
        }

        public void setLogin(Login login) {
            this.login = login;
        }


    }

    private class ResponseData {

        int stat;
        String mesg;

        public int getStat() {
            return stat;
        }

        public void setStat(int stat) {
            this.stat = stat;
        }

        public String getMesg() {
            return mesg;
        }

        public void setMesg(String mesg) {
            this.mesg = mesg;
        }



    }

    private void showDialog( String mesg,  String title) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(mesg)
                .positiveText("OK")
                .show();


    }

}
