package living.smarthack.smarthome.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import living.smarthack.smarthome.LoginActivity;
import living.smarthack.smarthome.R;
import living.smarthack.smarthome.backend.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private User user = User.getInstance();

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_profile, container, false);
        TextView  lblUserName  = view.findViewById(R.id.lblUsername) ;
        TextView  lblPhone = view.findViewById(R.id.lblPhone) ;
        TextView  lblEmail = view.findViewById(R.id.lblEmail) ;

        lblUserName.setText(user.login_name);
        lblPhone.setText(user.telephone);
        lblEmail.setText(user.email);

        Button btnLogout =  view.findViewById(R.id.btnLogout) ;
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.logout ();
                SharedPreferences pref = getActivity().getSharedPreferences(
                        getActivity().getPackageName() , Context.MODE_PRIVATE);
                pref.edit().putString("flag" , "").apply();

                getActivity().finish();


                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        return  view ;
    }

}
