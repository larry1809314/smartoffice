package living.smarthack.smarthome.adapter;


import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import living.smarthack.smarthome.R;
import living.smarthack.smarthome.model.AccessWorker;
import living.smarthack.smarthome.model.Device;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolders>  {

    private ArrayList<Device> itemList;
    private Context context;
    private int lastPosition = -1;

    public DashboardAdapter(Context context , ArrayList<Device> itemList) {
        this.itemList = itemList;
        this.context = context;

    }

    public void setList(ArrayList<Device> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public DashboardAdapter.ViewHolders onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layoutView  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, null);
        return new ViewHolders(layoutView);

    }

    @Override
    public void onBindViewHolder(@NonNull DashboardAdapter.ViewHolders viewHolders, int i) {
        Device element = itemList.get(i) ;

        int resourceId = R.drawable.buildings;
        ColorStateList csl;
        viewHolders.deviceName.setText( element.getDevice_type_name()) ;
        viewHolders.deviceDesp.setText( element.getDevice_name()) ;
        if (element.getDevice_type() == 1) {
            csl = AppCompatResources.getColorStateList(context, android.R.color.holo_green_dark);
            ImageViewCompat.setImageTintList(viewHolders.imgConnection, csl);
            resourceId = R.drawable.temperature;
            viewHolders.deviceStatus.setText( new java.text.DecimalFormat("###,#").format(element.getSensor_temperature()) + (char) 0x00B0 + "C") ;
        } else if (element.getDevice_type() == 2) {
            csl = AppCompatResources.getColorStateList(context, android.R.color.holo_green_dark);
            ImageViewCompat.setImageTintList(viewHolders.imgConnection, csl);
            resourceId = R.drawable.drop;
            viewHolders.deviceStatus.setText( new java.text.DecimalFormat("###,#").format(element.getSensor_humidity()) + "%") ;
        } else {
            if (element.getDevice_type() == 3) {
                resourceId = R.drawable.light;
                if (element.getDevice_stat() == 0) {
                    viewHolders.deviceStatus.setText("Status: OFF");
                    csl = AppCompatResources.getColorStateList(context, android.R.color.holo_red_dark);
                    viewHolders.cardView.setCardBackgroundColor(ContextCompat.getColor( context , R.color.very_light_gray));
                } else {
                    viewHolders.deviceStatus.setText("Status: ON");
                    csl = AppCompatResources.getColorStateList(context, android.R.color.holo_green_dark);
                    viewHolders.cardView.setCardBackgroundColor(ContextCompat.getColor( context , android.R.color.white));
                }
                ImageViewCompat.setImageTintList(viewHolders.imgConnection, csl);
            } else if (element.getDevice_type() == 4) {
                resourceId = R.drawable.key;
                if (element.getDevice_stat() == 0) {
                    viewHolders.deviceStatus.setText("Status: Open/Unlocked");
                    csl = AppCompatResources.getColorStateList(context, android.R.color.holo_red_dark);
                    viewHolders.cardView.setCardBackgroundColor(ContextCompat.getColor( context , R.color.very_light_gray));
                } else {
                    viewHolders.deviceStatus.setText("Status: Close/Locked");
                    csl = AppCompatResources.getColorStateList(context, android.R.color.holo_green_dark);
                    viewHolders.cardView.setCardBackgroundColor(ContextCompat.getColor( context , R.color.yellow));
                }
                ImageViewCompat.setImageTintList(viewHolders.imgConnection, csl);
            }

        }

        viewHolders.image.setImageResource(resourceId);



        //setAnimation(viewHolders.itemView,  i);
    }



    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void addItem(Device device) {
        itemList.add(itemList.size() - 1, device ) ;
        notifyDataSetChanged();
    }




    public class ViewHolders extends RecyclerView.ViewHolder {

        private ImageView image;
        private ImageView imgConnection;
        private TextView deviceName;
        private TextView deviceDesp ;
        private TextView deviceStatus ;
        private CardView cardView;


        public ViewHolders(View itemView) {
            super(itemView);
            deviceName =  itemView.findViewById(R.id.lblTitle);
            deviceDesp =  itemView.findViewById(R.id.lblDesp);
            deviceStatus =  itemView.findViewById(R.id.lblStatus);
            cardView =  itemView.findViewById(R.id.cardView );
            image = itemView.findViewById(R.id.imgType);

            imgConnection = itemView.findViewById(R.id.imgConnection);
        }

    }
}



