package living.smarthack.smarthome.model;


public class AccessWorker {
    private final long id;
    private String login_username;
    private String nfc_id;

    public AccessWorker (long id, String login_username  , String nfc_id ) {
        this.id = id;
        this.login_username = login_username;
        this.nfc_id = nfc_id ;
    }


    public AccessWorker () {
        this.id = 0 ;
        this.login_username = "";
        this.nfc_id = "" ;
    }


    public long getId() {
        return id;
    }

    public String getName () {
        return login_username;
    }

    public void  setName (String username ) {
        this.login_username = username;
    }


    public String getNfcId   () {
        return nfc_id ;
    }

    public void  setNfcId   ( String  nfcId ) {
        this.nfc_id = nfcId;
    }

    @Override
    public String toString() {
        return "Access Active Time : 1:00pm - 6:00pm";
    }

}

