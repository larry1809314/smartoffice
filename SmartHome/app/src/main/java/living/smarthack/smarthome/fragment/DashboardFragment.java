package living.smarthack.smarthome.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;

import living.smarthack.smarthome.DeviceActivity;
import living.smarthack.smarthome.LoginActivity;
import living.smarthack.smarthome.R;
import living.smarthack.smarthome.RegisterActivity;
import living.smarthack.smarthome.adapter.AccessWorkerAdapter;
import living.smarthack.smarthome.adapter.DashboardAdapter;
import living.smarthack.smarthome.adapter.RecyclerItemClickListener;
import living.smarthack.smarthome.adapter.RoomAdapter;
import living.smarthack.smarthome.adapter.StaticData;
import living.smarthack.smarthome.model.AccessWorker;
import living.smarthack.smarthome.model.Device;
import living.smarthack.smarthome.model.RequestData;
import living.smarthack.smarthome.model.ResponseData;
import living.smarthack.smarthome.model.Room;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    private ArrayList<Room> list = new ArrayList<Room> () ;
    private RoomAdapter adapter ;


    public DashboardFragment() {
        // Required empty public constructor
    }

    public static DashboardFragment newInstance() {
        Bundle args = new Bundle();
        DashboardFragment sampleFragment = new DashboardFragment();
        sampleFragment.setArguments(args);
        return sampleFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    protected  void fetchDevice() {
        list.clear();

        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            final Gson gson = gsonBuilder.create();
            RequestData requestData = new RequestData();
            requestData.setLogin_id(StaticData.loginID);

            String data = new Gson().toJson(requestData);

            byte[] datas = data.getBytes("UTF-8");
            String base64 = Base64.encodeToString(datas, Base64.DEFAULT);

            String url = StaticData.serverURL + "/RoomData?data=" + base64;
            AndroidNetworking.get(url)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String resp = response.toString();
                                Log.i("ASDASDASD", resp);
                                ResponseData responseData = new Gson().fromJson(resp,
                                        new TypeToken<ResponseData>(){}.getType());

                                if (responseData.getStat() == 1) {
                                    list = responseData.getRooms();
                                    adapter.setList(list);
                                    adapter.notifyDataSetChanged();
                                }

                            }catch ( Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        //Room w_device = new Room(  0 , "Room 1" , "Wehub Co-working space" , 0 , 0 ) ;
        //Room l_device = new Room(  0 , "Room 2" , "KC Accounting Firm" , 0 , 0 ) ;
        //Room d_device = new Room(  0 , "Room 3" , "Tesla" , 0 , 0 ) ;
        //list.add(w_device);
        //list.add(l_device);
        //list.add(d_device);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view  =  inflater.inflate(R.layout.fragment_dashboard, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.listDashboard) ;
        RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager( getActivity()) ;
        recyclerView.setLayoutManager( layoutManager );
        list.clear();
        adapter = new RoomAdapter(getActivity().getApplicationContext() , list );
        recyclerView.setAdapter(adapter);

        fetchDevice ();


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getApplicationContext(),
                        recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        Room  selectedDevice = list.get(position);
                        StaticData.roomID = Integer.parseInt(selectedDevice.getRoom_id());
                        Intent intent = new Intent(getActivity(), DeviceActivity.class);
                        startActivity( intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );




        final SwipeRefreshLayout swipeLayout = view.findViewById(R.id.layoutRefresh) ;
        swipeLayout.setRefreshing(false ) ;
        SwipeRefreshLayout.OnRefreshListener listener = new SwipeRefreshLayout.OnRefreshListener () {

            @Override
            public void onRefresh() {
                fetchDevice();
                adapter.notifyDataSetChanged();
                swipeLayout.setRefreshing(false ) ;
            }
        };
        swipeLayout.setOnRefreshListener(listener);

        return   view ;
    }

}
